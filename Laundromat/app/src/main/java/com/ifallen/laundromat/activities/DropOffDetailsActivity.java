package com.ifallen.laundromat.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.ifallen.laundromat.R;
import com.ifallen.laundromat.databinding.ActivityDropOffDetailsBinding;
import com.ifallen.laundromat.model.DropOff;
import com.ifallen.laundromat.utils.SharedPreferencesUtils;

public class DropOffDetailsActivity extends AppCompatActivity {

    private ActivityDropOffDetailsBinding binding;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        binding = ActivityDropOffDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        binding.backArrow.setOnClickListener(v -> onBackPressed());
        DropOff dropOff = (DropOff) getIntent().getSerializableExtra("dropOff");
        assert dropOff != null;

        //set data into views
        binding.nameTv.setText(dropOff.getName());
        binding.mobileTv.setText(dropOff.getMobile());
        binding.pickUpTv.setText(dropOff.getPickUpDate());
        binding.totalCostTv.setText(dropOff.getTotalCost());
        binding.advanceTv.setText(dropOff.getAdvance());
        binding.balanceDueTv.setText(dropOff.getBalanceDue());
        if (dropOff.getIsReady().equals("true")) {
            binding.readyForPickupTv.setText("Yes");
        } else {
            binding.readyForPickupTv.setText("No");
        }

        binding.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DropOffDetailsActivity.this);
                builder.setMessage("Are you sure you want to delete this item?")
                        .setPositiveButton("Yes", (dialog, id) -> {
                            // Perform delete operation here
                            SharedPreferencesUtils.deleteData(DropOffDetailsActivity.this, dropOff);
                            dialog.dismiss();
                            Toast.makeText(DropOffDetailsActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        })
                        .setNegativeButton("No", (dialog, id) -> dialog.dismiss());
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }
}