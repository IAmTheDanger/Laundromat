package com.ifallen.laundromat.model;

import java.io.Serializable;
import java.util.Objects;

public class DropOff implements Serializable {
    private String name;
    private String mobile;
    private String pickUpDate;
    private Long timestamp;
    private String totalCost;
    private String advance;
    private String balanceDue;
    private String isReady;

    // Constructor, getters, and setters

    public DropOff(String name, String mobile, String pickUpDate, String totalCost, String advance, String balanceDue, String isReady, Long timestamp) {
        this.name = name;
        this.mobile = mobile;
        this.pickUpDate = pickUpDate;
        this.totalCost = totalCost;
        this.advance = advance;
        this.balanceDue = balanceDue;
        this.isReady = isReady;
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getAdvance() {
        return advance;
    }

    public void setAdvance(String advance) {
        this.advance = advance;
    }

    public String getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(String balanceDue) {
        this.balanceDue = balanceDue;
    }

    public String getIsReady() {
        return isReady;
    }

    public void setIsReady(String isReady) {
        this.isReady = isReady;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DropOff other = (DropOff) o;
        // Compare the fields that define equality for DropOff objects
        return Objects.equals(this.name, other.name) &&
                Objects.equals(this.mobile, other.mobile);
        // Add more fields if necessary
        // Repeat this pattern for all fields
        // Example: Objects.equals(this.fieldN, other.fieldN);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, mobile /*, other fields if any*/);
    }
}
