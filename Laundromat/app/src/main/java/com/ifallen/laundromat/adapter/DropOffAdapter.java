package com.ifallen.laundromat.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ifallen.laundromat.activities.DropOffDetailsActivity;
import com.ifallen.laundromat.databinding.ItemDropOffBinding;
import com.ifallen.laundromat.model.DropOff;

import java.util.List;

public class DropOffAdapter extends RecyclerView.Adapter<DropOffAdapter.UserViewHolder> {

    private List<DropOff> dropOff;
    private Context context;

    // Constructor to initialize the adapter with dropOff data and context
    public DropOffAdapter(List<DropOff> dropOff, Context context) {
        this.dropOff = dropOff;
        this.context = context;
    }

    // Inflating layout and creating ViewHolder
    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate the layout for each item using data binding
        ItemDropOffBinding binding = ItemDropOffBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new UserViewHolder(binding);
    }

    // Binding data to ViewHolder
    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        // Get the DropOff object at the specified position
        DropOff dropOffItem = dropOff.get(position);
        // Bind the DropOff data to the ViewHolder
        holder.bind(dropOffItem);
    }

    // Return the size of the dataset
    @Override
    public int getItemCount() {
        return dropOff.size();
    }

    // ViewHolder class to hold reference to the views for each data item
    public class UserViewHolder extends RecyclerView.ViewHolder {
        private ItemDropOffBinding binding;

        public UserViewHolder(@NonNull ItemDropOffBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        // Bind method to set data to the views
        public void bind(DropOff dropOff) {
            // Set DropOff data to the views
            binding.nameTv.setText(dropOff.getName());
            binding.pickUpTv.setText(dropOff.getPickUpDate());
            // Set the checkbox based on the value of isReady
            binding.pickUpCheckBox.setChecked(dropOff.getIsReady().equals("true"));

            // Set OnClickListener to open DropOffDetailsActivity
            binding.dropOffCard.setOnClickListener(v -> {
                // Create intent to open DropOffDetailsActivity
                Intent intent = new Intent(context, DropOffDetailsActivity.class);
                // Pass DropOff object to the activity
                intent.putExtra("dropOff", dropOff);
                // Start DropOffDetailsActivity
                context.startActivity(intent);
            });
        }
    }
}

