package com.ifallen.laundromat.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.ifallen.laundromat.R;
import com.ifallen.laundromat.databinding.ActivityAddDropOffBinding;
import com.ifallen.laundromat.model.DropOff;
import com.ifallen.laundromat.utils.SharedPreferencesUtils;

import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

public class AddDropOffActivity extends AppCompatActivity {

    private ActivityAddDropOffBinding binding;
    private long pickUpTimestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        binding = ActivityAddDropOffBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        //to go back
        binding.backArrow.setOnClickListener(v -> onBackPressed());

        binding.pickUpDateEt.setOnClickListener(v -> {
            // Create a DatePickerDialog instance
            DatePickerDialog datePickerDialog;

            // Get the current date
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

            // Initialize the DatePickerDialog with the current date and a listener for date selection
            datePickerDialog = new DatePickerDialog(AddDropOffActivity.this, (datePicker, selectedYear, selectedMonth, selectedDayOfMonth) -> {
                // Format the selected date
                String formattedDayOfMonth = String.format(Locale.getDefault(), "%02d", selectedDayOfMonth);
                String formattedMonth = String.format(Locale.getDefault(), "%02d", selectedMonth + 1);
                String selectedDate = formattedDayOfMonth + "-" + formattedMonth + "-" + selectedYear;

                // Set the formatted date to the EditText
                binding.pickUpDateEt.setText(selectedDate);

                // Create a Calendar instance with the selected date
                Calendar selectedCalendar = Calendar.getInstance();
                selectedCalendar.set(selectedYear, selectedMonth, selectedDayOfMonth);

                // Get the timestamp of the selected date in milliseconds
                pickUpTimestamp = selectedCalendar.getTimeInMillis();

            }, year, month, dayOfMonth);

            // Show the DatePickerDialog
            datePickerDialog.show();
        });

        binding.addBtn.setOnClickListener(v -> {
            if (validateForm()) {
                // Add your code to save the drop-off details to the database here
                SharedPreferencesUtils.saveDropOffData(AddDropOffActivity.this,
                        new DropOff(
                                Objects.requireNonNull(binding.nameEt.getText()).toString().trim(),
                                Objects.requireNonNull(binding.mobileEt.getText()).toString().trim(),
                                Objects.requireNonNull(binding.pickUpDateEt.getText()).toString(),
                                Objects.requireNonNull(binding.totalCostTv.getText()).toString().trim(),
                                Objects.requireNonNull(binding.advanceTv.getText()).toString().trim(),
                                Objects.requireNonNull(binding.balanceDueTv.getText()).toString().trim(),
                                String.valueOf(binding.readyForPickupCb.isChecked()),
                                pickUpTimestamp
                        ));

                Toast.makeText(this, "Drop off data added successfully", Toast.LENGTH_SHORT).show();

                onBackPressed();
            }
        });

    }

    public boolean validateForm() {
        boolean isValid = true;
        // Validate Name
        String name = Objects.requireNonNull(binding.nameEt.getText()).toString().trim();
        if (name.isEmpty()) {
            binding.nameEt.setError("Please enter your name");
            isValid = false;
        }

        // Validate Mobile
        String mobile = Objects.requireNonNull(binding.mobileEt.getText()).toString().trim();
        if (mobile.isEmpty()) {
            binding.mobileEt.setError("Please enter your mobile number");
            isValid = false;
        }

        // Validate Pick-up Date
        String pickUpDate = Objects.requireNonNull(binding.pickUpDateEt.getText()).toString().trim();
        if (pickUpDate.isEmpty()) {
            binding.pickUpDateEt.setError("Please select pick-up date");
            isValid = false;
        }

        // Validate Total Cost
        String totalCost = Objects.requireNonNull(binding.totalCostTv.getText()).toString().trim();
        if (totalCost.isEmpty()) {
            binding.totalCostTv.setError("Please enter total cost");
            isValid = false;
        }

        // Validate Advance
        String advance = Objects.requireNonNull(binding.advanceTv.getText()).toString().trim();
        if (advance.isEmpty()) {
            binding.advanceTv.setError("Please enter advance");
            isValid = false;
        }

        // Validate Balance Due
        String balanceDue = Objects.requireNonNull(binding.balanceDueTv.getText()).toString().trim();
        if (balanceDue.isEmpty()) {
            binding.balanceDueTv.setError("Please enter balance due");
            isValid = false;
        }

        return isValid;
    }
}