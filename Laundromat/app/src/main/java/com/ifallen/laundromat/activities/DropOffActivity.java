package com.ifallen.laundromat.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ifallen.laundromat.R;
import com.ifallen.laundromat.adapter.DropOffAdapter;
import com.ifallen.laundromat.databinding.ActivityDropOffBinding;
import com.ifallen.laundromat.model.DropOff;
import com.ifallen.laundromat.utils.SharedPreferencesUtils;

import java.util.Comparator;
import java.util.List;

public class DropOffActivity extends AppCompatActivity {

    private ActivityDropOffBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        binding = ActivityDropOffBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        binding.backArrow.setOnClickListener(v -> onBackPressed());


        binding.addDropOffFab.setOnClickListener(v -> startActivity(new Intent(DropOffActivity.this, AddDropOffActivity.class)));


    }

    @Override
    protected void onResume() {
        super.onResume();

        // Retrieve drop-off data list from SharedPreferences
        List<DropOff> dataList = SharedPreferencesUtils.getDropOffDataList(DropOffActivity.this);

        if (dataList != null && !dataList.isEmpty()) {
            // Sort the dataList by timestamp
            dataList.sort(new Comparator<DropOff>() {
                @Override
                public int compare(DropOff dropOff1, DropOff dropOff2) {
                    // Compare timestamps of the two DropOff objects
                    return Long.compare(dropOff1.getTimestamp(), dropOff2.getTimestamp());
                }
            });

            // If dataList is not null and not empty, set up RecyclerView with adapter
            DropOffAdapter adapter = new DropOffAdapter(dataList, DropOffActivity.this);
            binding.dropOffRv.setAdapter(adapter);
            binding.dropOffRv.setLayoutManager(new LinearLayoutManager(DropOffActivity.this));
        }
    }

}