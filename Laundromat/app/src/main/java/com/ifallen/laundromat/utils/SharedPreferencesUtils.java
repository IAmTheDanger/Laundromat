package com.ifallen.laundromat.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ifallen.laundromat.model.DropOff;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPreferencesUtils {

    // Constants for SharedPreferences keys
    private static final String PREF_NAME = "form_data_prefs";
    private static final String KEY_FORM_DATA_LIST = "form_data_list";

    // Private constructor to prevent instantiation
    private SharedPreferencesUtils() {
    }

    // Save drop-off data list to shared preferences

    public static void saveDropOffData(Context context, DropOff dropOffData) {
        // Get the current list of DropOff objects from SharedPreferences
        List<DropOff> dropOffDataList = getDropOffDataList(context);

        // Add the new DropOff object to the list
        if (dropOffDataList == null) {
            dropOffDataList = new ArrayList<>();
        }
        dropOffDataList.add(dropOffData);

        // Save the updated list to SharedPreferences
        saveDropOffDataList(context, dropOffDataList);
    }

    public static void saveDropOffDataList(Context context, List<DropOff> dropOffDataList) {
        // Get SharedPreferences editor
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();

        // Convert drop-off data list to JSON string using Gson
        Gson gson = new Gson();
        String json = gson.toJson(dropOffDataList);

        // Save JSON string to SharedPreferences
        editor.putString(KEY_FORM_DATA_LIST, json);
        editor.apply();
    }

    // Retrieve drop-off data list from shared preferences
    public static List<DropOff> getDropOffDataList(Context context) {
        // Get SharedPreferences instance
        SharedPreferences sharedPreferences = getSharedPreferences(context);

        // Retrieve JSON string from SharedPreferences
        String json = sharedPreferences.getString(KEY_FORM_DATA_LIST, null);

        // Convert JSON string to list of DropOff objects using Gson
        Type type = new TypeToken<ArrayList<DropOff>>() {}.getType();
        Gson gson = new Gson();
        return gson.fromJson(json, type);
    }

    // Delete a drop-off data object from the list
    public static void deleteData(Context context, DropOff dropOffDataToDelete) {
        // Get the drop-off data list
        List<DropOff> dropOffDataList = getDropOffDataList(context);

        Log.e("TAG", "deleteData: " + dropOffDataToDelete.toString());
        // Check if the list is not null
        if (dropOffDataList != null) {
            // Remove the specified drop-off data object
            dropOffDataList.remove(dropOffDataToDelete);

            Log.e("TAG", "deleteData: " + dropOffDataList.size());

            // Save the updated list to SharedPreferences
            saveDropOffDataList(context, dropOffDataList);
        }else {
            Log.e("TAG", "deleteData: List is null");
        }
    }

    // Get SharedPreferences instance
    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }
}

